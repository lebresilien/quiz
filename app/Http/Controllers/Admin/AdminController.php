<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Models\{Setting, Event, Quiz, User };
use Illuminate\Support\Facades\DB;
use DataTables;
use Session;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function dashboard()
    {
        $setting = Setting::take(1)->first();
        return view('admin.index', compact('setting'));
    }

    public function editSetting()
    {
        $setting = Setting::take(1)->first();
        return view('admin.setting', compact('setting'));
    }

    public function updateSetting(Request $request)
    {

        Validator::make(
            $request->all(),
            [
                'question' => ['required', 'integer', 'gt:0'],
                'answer' => ['required', 'integer', 'gt:0'],
            ]
        )->validate();

       $setting  = Setting::findOrFail($request->id);
       $setting->question = $request->question;
       $setting->answer = $request->answer;
       $setting->save();

      return redirect()->route('dashboard')->withInput();
       
    }

    public function accesystem(Request $request)
    {
        $event = Event::where('name', $request->event)->first();
        if(!$event)
        {
            Session::flash('message', "Event doesn't found or expired!!");
            return Redirect()->back();
        }
        return redirect()->route('approved');
    }

    public function approved()
    {
        return view('clients.clientwait');
    }

    public function createEvent()
    {  
        $setting = Setting::take(1)->first();
        return view('admin.events.create', compact('setting'));
    }

    public function eventStore(Request $request)
    {
        DB::beginTransaction();

        try 
        {
           
            // saving events
            $event = Event::create([
                'name' => $request->name,
                'timer' => $request->timer,
                'started_at' => $request->datetime,
            ]);

            // extract event item
            $collection_explode = explode("|", $request->collection);
      
            for($i=0; $i < (sizeof($collection_explode) - 1); $i++)
            {
                $collection_item = explode(";", $collection_explode[$i]);
                // extract quizz questions
                $question = explode(",", $collection_item[0])[0];
                // extract quizz good_answer
                $good_answer = explode(",", $collection_item[sizeof($collection_item)-1])[0];
                $answer = "";

                // extract quizz differents answers
                for($j=1; $j < sizeof($collection_item)-1; $j++)
                {
                  $answer .= $collection_item[$j];
                }
            
                Quiz::create([
                    'event_id' => $event->id,
                    'question' => $question,
                    'answer' => $answer,
                    'good_answer' => $good_answer
               ]);
                
            }
            
            DB::commit();
            return response()->json($event);
            
        } catch (\Exception $e) {
            DB::rollback();
            
        }
    }

    public function index()
    {
        return view('admin.events.index');
    }

    public function data(Request $request)
    {
        if($request->ajax()) {
            $data = $events = Event::with('quizs')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="' . route('events.show', $row->id) . '" class="show btn btn-success btn-sm">Show</a> <a href="' . route('events.alert', $row->id) . '" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public  function eventSearch($name)
    {
        $event = Event::with('quizs')->where('name', $name)->first();
    }

    public function destroyEvent($id)
    {
        $user = Auth::user();
        if(!$user->hasRole('admin')) {
            abort(404);
        }

        $event = Event::findOrFail($id);
        if(!$event) return;

        Quiz::where('event_id', $event->id)->delete();
        $event->delete();
        Session::flash('message', "Event delected succefully!");
        return redirect()->route('events.index');
    }


    public function subscribe(Request $request)
    {
  
        Validator::make($request->all(), [
            'fname' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
        ])->validate();

        $user = User::create([
            "fname" => $request->fname,
            "lname" => $request->lname,
            "email" => $request->email,
            'password' => Hash::make($request->email)
        ]);

        Session::flash('message', "Whoauh!! Suscribe succefully. Wait on countdown over");
        return Redirect()->back();
    }

    public function showEvent($id)
    {
        $event = Event::with('quizs')->where('id', $id)->first();
        return view('admin.events.show', compact('event'));
    }

    public function alert($id)
    {
        $event = Event::findOrFail($id);
        return view('admin.events.destroy', compact('event'));
    }
}
