<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'timer',
        'started_at',
        'status'
    ];

    public function quizs()
    {
        return $this->hasMany(Quiz::class);
    }
}
