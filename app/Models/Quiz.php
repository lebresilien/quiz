<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use HasFactory;

    protected  $table = 'quizs';
    protected $fillable = [
        'event_id',
        'question',
        'answer',
        'good_answer'
       
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
