@extends('clients.layout')
@section('sectionTime')

@if (Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif

<div id="cadreTime">

        <div class="card" style="width: 18rem;">
            <div class="card-body">
                    <p class="card-text">
                        <span class="cssTimer">H :</span>
                    </p>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <div class="card-body">
                    <p class="card-text">
                        <span class="cssTimer">Min : </span>
                    </p>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <div class="card-body">
                    <p class="card-text">
                        <span class="cssTimer">Sec : </span>
                    </p>
            </div>
        </div>

</div>
@endsection


@section('sectionFormUser')
<div id="cadre_form">
    <form action="{{ route('subscribe') }}" method="POST" >
        @csrf
        <div class="form-group">
            <label for="formGroupExampleInput" class="forLabel">Last name</label>
            <input type="text" name="lname" class="form-control" id="formGroupExampleInput" placeholder="eg:John">
        </div>

        <div class="form-group">
            <label for="formGroupExampleInput2" class="forLabel">First name</label>
            <input type="text" required name="fname" class="form-control @error('fname') is-invalid @enderror" id="formGroupExampleInput2" placeholder="eg:Doe">
            @error('fname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="formGroupExampleInput2" class="forLabel">Email</label>
            <input type="email" required name="email" class="form-control @error('email') is-invalid @enderror" id="formGroupExampleInput2" placeholder="eg:johndoe@gmail.com">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary my-1 mt-3 " style="float:right">Send</button>

    </form>
</div>
@endsection