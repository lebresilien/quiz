<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">

    @yield('css')
</head>
<body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="page page-center">
        <div class="container-tight py-4">
            <div class="text-center mb-4">
                <a href="/"><img src="{{ asset('public/static/logo.svg') }}" height="86" alt=""></a>
            </div>
            @yield('content')
        </div>
    </div>
    <script src="{{ asset('public/js/app.js') }}"></script>
    @yield('js')
</body>
</html>
