@extends('auth.layout')

@section('content')
    <form class="card card-md" action="{{ route('login') }}" method="POST" autocomplete="off">
        @csrf
        <div class="card-body">
            <h2 class="card-title text-center mb-4">Login to your account</h2>
            <div class="mb-3">
                <label class="form-label">Email address</label>
                <input 
                       type="email" 
                       name="email"
                       class = 'form-control @error('email') is-invalid @enderror'
                       value ="{{ old('email') }}"
                       placeholder="Enter email"
                       autofocus
                       required
                >
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mb-2">

                <label class="form-label">
                    Password
                    <span class="form-label-description">
                        <a href="#">I forgot password</a>
                    </span>
                </label>

                <div class="input-group input-group-flat">
                    <input 
                            type="password" 
                            name="password"
                            class='form-control  @error('password') is-invalid @enderror'  
                            placeholder="Password" 
                            required
                            autocomplete="current-password"
                    >
                   
                    <span class="input-group-text">
                        <a href="#" class="link-secondary" title="Show password" data-bs-toggle="tooltip"><!-- Download SVG icon from http://tabler-icons.io/i/eye -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="12" cy="12" r="2" /><path d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" /></svg>
                        </a>
                    </span>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

            </div>
            <div class="mb-2">
                <label class="form-check">
                    <input 
                            type="checkbox" 
                            class="form-check-input"
                            name="remember"
                            id="remember" {{ old('remember') ? 'checked' : '' }}
                    />
                    <span class="form-check-label">Remember me</span>
                </label>
            </div>
            <div class="form-footer">
                <button type="submit" class="btn btn-primary w-100">Sign in</button>
            </div>
        </div>
    </form>
@endsection
