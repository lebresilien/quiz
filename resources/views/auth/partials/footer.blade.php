<div class="container-fluid py-4" style="background:black">


    <div class="d-flex flex-column flex-sm-row justify-content-sm-between mx-4 align-items-md-center">
        
       <div class="px-5 mx-5 mx-md-0 px-md-0">
            <a href="https://facebook.com/promosnature">
               <i class="icon mdi mdi-facebook" style="color:white"></i>
            </a>
            <a href="https://twitter.com/promosnature">
               <i class="icon mdi mdi-twitter" style="color:white"></i>
            </a>
            <a href="https://youtube.com/promosnature">
               <i class="icon mdi mdi-youtube" style="color:white"></i>
            </a>
            <a href="https://instagram.com/promosnature">
               <i class="icon mdi mdi-instagram" style="color:white"></i>
            </a>
            <a href="https://pinterest.com/promosnature">
               <i class="icon mdi mdi-pinterest" style="color:white"></i>
            </a>
         <!--  <i class="icon mdi mdi-dribbble" style="color:white"></i>
          <i class="icon mdi mdi-behance"style="color:white"></i> -->
       </div>

       <div class="">
            <p style="font-weight:bold;color:white">
                Copyright © 2021 | Créé par <span style="color:#f09a1f" target="_blank"><a href="https://www.dv-digital.dreamwpro.com">Dream-W pro</a> <span>
            </p>
       </div>

    </div>
    
</div>