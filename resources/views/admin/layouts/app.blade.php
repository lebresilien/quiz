<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
      <title>@yield('title', config('app.name'))</title>
      <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
      <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
      
      @yield('styles')
  </head>
  <body>

    <div id="app">

      <div class="main-wrapper">
        @include('admin.layouts.partials.nav')
        
        <!-- Main Content -->
        <div class="offset-md-2" style="min-height: 547px;">
                  <div class="container">
                      <div class="row">
                          <div class="card py-2">
                            <h1>@yield('page_title')</h1>
                          </div>
                      </div>
                  </div>
              <div class="section-body">
                    @yield('content')
              </div>
          </div>

        @include('admin.layouts.partials.footer')
      </div>
    </div>
    
    <script src="{{ asset('public/js/app.js') }}"></script>
    <script src="{{ asset('public/js/jquery-3.3.1.min.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    
    @yield('scripts')

  </body>
</html>
