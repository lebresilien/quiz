<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; {{ date('Y') }} <div class="bullet"></div> Design By <a href="https://gitlab.com/lebresilien">DreamWPro</a>
    </div>
    <div class="footer-right"></div>
</footer>