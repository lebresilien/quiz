@extends('admin.layouts.app')
@section('page_title') Settings @endsection 
@section('content')

<div class="page-body">
    <div class="container-xl">
        <div class="row row-deck row-cards offset-md-1">
            <div class="col-md-9 offset-md-1">
            <form class="card card-md" action="{{ route('setting.update') }}" method="POST" >
                <div class="col-md-6 offset-md-3 py-5">
                @csrf
                <div class="mb-3">
                    <label class="form-label">Number of question</label>
                    <input 
                        type="text" 
                        class='form-control @error('question') is-invalid @enderror' 
                        name="question"
                        placeholder="Number of question"
                        value="{{ $setting->question }}"
                    >
                    @error('question')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label">Number of answer</label>
                    <input 
                        type="text" 
                        class='form-control @error('answer') is-invalid @enderror'
                        name="answer" 
                        placeholder="Number of answer"
                        value="{{ $setting->answer }}"
                    >
                    <input type="hidden" name="id" value="{{ $setting->id }}" />
                    @error('answer')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="row my-2">
                    <div class="col-xs-12">
                        <div class="text-right">
                            <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </div>
               </div>
               </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection