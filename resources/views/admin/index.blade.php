@extends('admin.layouts.app')
@section('page_title') Home @endsection
@section('content')

  <div class="page-body">
    <div class="container-xl">
        <div class="row row-deck row-cards">
        <div class="col-md-10 offset-md-1">
           <div class="col-6 col-md-3">
               <div class="card">
                   <div class="card-body">
                       <div class="card-body">
                           <div class="subheader">Quiz</div>
                           <div class="h1 mb-3">77</div>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-6 col-md-3">
               <div class="card">
                   <div class="card-body">
                       <div class="card-body">
                           <div class="subheader">Question per quiz</div>
                           <div class="h1 mb-3">
                               {{ $setting->question}}
                           </div>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-6 col-md-3">
               <div class="card">
                   <div class="card-body">
                       <div class="card-body">
                           <div class="subheader">Answer per question</div>
                           <div class="h1 mb-3">
                               {{ $setting->answer }}
                           </div>
                       </div>
                   </div>
               </div>
           </div>

           <div class="col-6 col-md-3">
               <div class="card">
                   <div class="card-body">
                       <div class="card-body">
                           <div class="subheader">Users</div>
                           <div class="h1 mb-3">100</div>
                       </div>
                   </div>
               </div>
           </div>
        </div>
        </div>
    </div>
</div>
@endsection