@extends('admin.layouts.app')
@section('page_title') New Events @endsection 
@section('content')

<div class="page-body">
    <div class="container-xl">
        <div class="row row-deck row-cards offset-md-1">
            <div class="col-md-9 offset-md-1">
                <div class="card card-md">
                    <div class="col-md-8 offset-md-2 py-4">

                        <div class="form-group row mb-3">
                            <label class="col-sm-4 col-form-label">Event name</label>
                            <div class="col-sm-8">
                                <input 
                                    type="text" 
                                    class='form-control' 
                                    name="name"
                                    placeholder="Number of question"
                                >
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label class="col-sm-4 col-form-label">Quiz time and date</label>
                            <div class="col-sm-8">
                                <input 
                                    type="datetime-local" 
                                    class='form-control' 
                                    name="datetime"
                                >
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label class="col-sm-4 col-form-label">Time for each question</label>
                            <div class="col-sm-8">
                                <input 
                                    type="number" 
                                    class='form-control' 
                                    name="timer"
                                    min="1.0"
                                >
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>

        <div class="row row-cards offset-md-1 mt-2">
            <div class="col-md-9 offset-md-1">
                <div class="card card-md">
                    <div class="col-md-8 offset-md-2 pt-2">
                        <p class="text-center">Question and answers</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-cards offset-md-1 mt-2">
            <div class="col-md-9 offset-md-1">
                <div class="card card-md">
                    <div class="col-md-8 pt-2">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                   <th class="text-nowrap"> Question</th>
                                    @for($i = 0; $i < $setting->answer; $i++)
                                      <th class="text-nowrap"> Answer {{ $i+1 }}</th>
                                      <th></th>
                                    @endfor
                                </tr>

                                @for($k = 0; $k < $setting->question; $k++)
                                    <tr name="tr{{$k}}" tr="tr">
                                        <td>
                                            <input type="text" placeholder="question" size="12" name="question"/>
                                        </td>
                                        @for($j = 0; $j < $setting->answer; $j++)
                                            <td>
                                               <input type="text"  placeholder="answer" size="12" texteinput="texteinput{{$k}}"/>
                                            </td>
                                            <td>
                                               <input type="radio" name="radio{{$k}}" value="{{$k}}" texteradio="texteradio{{$k}}"/>
                                            </td>
                                        @endfor
                                    </tr>
                                @endfor
                            </thead>

                            <tbody>
                               
                            </tbody>
                        </table>
                        
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-3">
                        <button type="button" class="btn btn-primary circle" id="save">
                            Save
                        </button>
                    </div>
                <div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('scripts')

   <script>
       $(function(){

            var taille_tableau = $('[tr="tr"]').length
            const token = $('meta[name="csrf-token"]').attr('content')

            $('#save').on('click', function(){

                var chaine_tableau = ""
                var question = ""
                var input_text = ""
                var chaine_input_text = ""
                var reponse = ""
                const name = $('[name="name"]').val()
                const datetime = $('[name="datetime"]').val()
                const timer = $('[name="timer"]').val()
                const date = new Date();
                const currentDate = date.getFullYear()+'/'+ parseInt(date.getMonth() + 1 )+'/'+ date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':00'
                
                const tab_date = datetime.split('T')[0]
                const split_year = tab_date.split('-')[0]
                const split_month = tab_date.split('-')[1]
                const split_day = tab_date.split('-')[2]
                const tab_time = datetime.split('T')[1]+':00'

                const submittingDate = split_year+'/'+split_month+'/'+split_day+ ' ' +tab_time
                
            
                if(name.length == 0 || datetime.length == 0 || timer.length == 0)
                {
                
                    Swal.fire({
                        title: 'Error!',
                        text: 'Fill the required field',
                        icon: 'error',
                        confirmButtonText: 'ok'
                    })
                    return ;
                }

                if(Date.parse(currentDate) > Date.parse(submittingDate))
                {
                    Swal.fire({
                        title: 'Error!',
                        text: 'Event date must be great than current date ',
                        icon: 'error',
                        confirmButtonText: 'ok'
                    })
                    return ;
                }

                

                for ( var i = 0 ; i < taille_tableau ; i++ )
                {
                    question = $('[name="tr'+i+'"] [name="question"]').val()
                    chaine_input_text = ""

                    $('[texteinput="texteinput'+i+'"]').each(function() 
                    {
                        input_text = $(this).val()
                        chaine_input_text += input_text+","
                    })

                    $('[texteradio="texteradio'+i+'"]').each(function() 
                    {
                        if($(this).prop('checked'))
                        {
                        reponse = $(this).parent().prev().children().val()
                        }
                    })

                    chaine_tableau += question+";"+chaine_input_text+";"+reponse+"|"
                }
               
                saveEvent(name, datetime, timer, chaine_tableau, token)
                
                
           })
       })

        function saveEvent(name, datetime, timer, collection, token)
        {
           $('#save').text('Saving...')
           const xhr = new XMLHttpRequest();
           const url = "{{ route('events.store') }}"

           xhr.open("POST", url, true)
           xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")

            xhr.onreadystatechange = function()
            {
                if(this.readyState === XMLHttpRequest.DONE && this.status === 200) 
                {
                    $('#save').text('Save')
                    const info = xhr.responseText;
                    const json = eval('('+info+')')
                    if(json.name)
                    {
                        location.href = './index'
                    }
                    else 
                    {
                        Swal.fire({
                        title: 'Error!',
                        text: 'An error ocurred',
                        icon: 'error',
                        confirmButtonText: 'ok'
                    })
                    }
                }
            }
            
            xhr.send("name="+name+"&datetime="+datetime+"&timer="+timer+"&collection="+collection+"&_token="+token);
            
        }
   </script>
@endsection