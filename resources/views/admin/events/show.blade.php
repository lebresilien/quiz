@extends('admin.layouts.app')
@section('page_title')  Event Details : {{ $event->name }} @endsection 
@section('content')

    <div class="page-body">
        <div class="container-xl">
            <div class="col-auto">
                @forelse($event->quizs as $quiz)
                    <div class="d-flex flex-row justify-content-between align-items-center mt-5">
                        
                    <div class="d-flex flex-column">
                            <p>Question</p>
                            <p class="font-weight-bold">{{ $quiz->question }}</p>
                        </div>

                        <div class="d-flex flex-column">
                           <p>Possible Answer</p>
                            @for($i=0; $i < count(explode(',', $quiz->answer)) -1 ; $i++)
                                <div class="p-1 font-weight-bold">
                                   {{explode(',',$quiz->answer)[$i]}}
                                </div>
                            @endfor
                        </div>

                        <div class="d-flex flex-column">
                            <p>Valid Answer</p>
                            <p class="font-weight-bold">{{ $quiz->good_answer }}</p>
                        </div>
                    </div>
                @empty
                    <p> no content </p>
                @endforelse

            </div>
        </div>
    </div>
@endsection