@extends('admin.layouts.app')
@section('page_title') List Events @endsection 
@section('content')

   <div class="col-auto ms-auto d-print-none">
        <div class="btn-list">
            
            <a href="{{ route('events.create') }}" class="btn btn-primary d-none d-sm-inline-block">
            <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
            Create new report
            </a>
            
        </div>
    </div>

    <div class="page-body">
        <div class="container-xl">
            <div class="row row-deck row-cards">
              
                <table class="table table-bordered yajra-datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>started_at</th>
                            <th>Timer</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

   <script>
      $(function(){
         
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('events.data') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'started_at', name: 'started_at'},
                {data: 'timer', name: 'timer'},
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: true, 
                    searchable: true
                },
            ]
        });
       
        const el = document.getElementsByClassName('delete')
            for (var i = 0 ; i < el.length; i++) {
               el[i].addEventListener('click' , showComment , false ) ; 
            }

            
        
      })
   </script>

   <script>
        $(function(){
           
        })
    </script>
@endsection