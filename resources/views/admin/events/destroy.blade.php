@extends('admin.layouts.app')
@section('page_title') Confirm delete @endsection 
@section('content')

<div class="container-fluid mt-5"> 
    <form id="deleteevent" action="{{ route('events.destroy', $event->id) }}" method="POST" style="display: none;">
      @csrf
      @method('DELETE')
    </form>
    <div class="row">
      <div class="col-sm-12 col-md-6 offset-md-3 col-lg-4 offset-lg-4">
        <div class="card text-white bg-dark mb-3">
          <div class="card-body">
            <h5 class="card-title text-center mb-3">You are about to delete event "<strong>{{ $event->name }}</strong>"</h5>
            <p class="card-text">
              <a class="btn btn-danger btn-lg btn-block" href="#" role="button"
              onclick="event.preventDefault(); 
              $('#deleteevent').submit();"
              >
                I confirm delete
            </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection