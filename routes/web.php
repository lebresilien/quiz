<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth'], 'namespace' => 'App\Http\Controllers\Admin'], function () {
    Route::get('dashboard', [AdminController::class, 'dashboard'])->name('dashboard');
    Route::get('events/index', [AdminController::class, 'index'])->name('events.index');
    Route::get('events/show/{id}', [AdminController::class, 'showEvent'])->name('events.show');
    Route::get('events/data', [AdminController::class, 'data'])->name('events.data');
    Route::get('events/create', [AdminController::class, 'createEvent'])->name('events.create');
    Route::get('setting/edit', [AdminController::class, 'editSetting'])->name('setting.edit');
    Route::post('setting/update', [AdminController::class, 'updateSetting'])->name('setting.update');
    Route::post('events/store', [AdminController::class, 'eventStore'])->name('events.store');
    Route::delete('events/destroy/{id}', [AdminController::class, 'destroyEvent'])->name('events.destroy');
    Route::get('events/alert/{id}', [AdminController::class, 'alert'])->name('events.alert');
    Route::get('events/search/{name}', [AdminController::class, 'eventSearch'])->name('events.search');
});

Route::get('accesapproved', [AdminController::class, 'approved'])->name('approved');
Route::post('accesystem', [AdminController::class, 'accesystem'])->name('accesystem');
Route::post('subcribe', [AdminController::class, 'subscribe'])->name('subscribe');

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cleared!";
});