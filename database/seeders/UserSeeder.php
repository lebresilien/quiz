<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\Models\{User, Setting};

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('users')->delete();

        $adminRole = Role::create(['name' => 'admin']);
        $admin = User::create([
            'fname' => 'John Doe',
            'lname' => '',
            'email' => 'john.doe@gmail.com',
            'password' => Hash::make('12345678'),
        ]);

        $admin->assignRole($adminRole->id);

        Setting::create([
            'question' => 5,
            'answer' => 3
        ]);
    }
}
